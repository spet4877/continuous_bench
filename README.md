# Continuous BENCH for UKBB  

## Installation

This assumes you have conda (https://docs.conda.io/en/latest/) already installed.

1. Git clone this repository

```
git clone https://git.fmrib.ox.ac.uk/spet4877/continuous_bench.git
```

2. Create a new conda environment

```
conda create -n cbench_env
conda activate cbench_env
```

3. Navigate to directory and install necessary packages

**TODO: generate new requirement.txt file**

```
cd continuous_bench
conda install --file requirements.txt --channel bioconda
```

## How to use it?
This library assumes you have basic knowledge of [BENCH](https://git.fmrib.ox.ac.uk/hossein/bench).
 
This library modifies a version of BENCH to work with continuous variables (named "**Continuous BENCH**")

Here, we specifically apply it to UKBB data. The scripts below are found in the **scripts** directory. 

Continuous BENCH is applied using four main steps:

### 1. Train change models:
This step is for training change models for parameters of a specific biophysical model with a given acquisition protocol. 

Here, I trained it on **jalapeno (i.e. WIN computing cluster)**. 

To train models of change you need to run the following command:

```
bash training_invivo_models_for_UKBB_2D.sh
```

Note: You can change the inputs for your change models in the bash script itself. 

### 2. Compute summary measurements:
From this point, all jobs are run on **BMRC**, since we are working with UK BB data. 

This stage estimates rotationally invariant summary measurements from dMRI data for each subject. All subjects should have the same b-values, which are used for training models of change, but the b-vectors and ordering of measurements can vary across subjects.
Run the following command to estimate the summary measurements:

```
bash bmrc_slurm_summary_measures.sh
```
**Inputs required in bash script:**
```
data #raw dMRI data of different UKBB subjects.
bvecs #b-vector of the dMRI data.
bvals #b-value of the dMRI data.
warp #Transformation from dMRI to MNI space.
mask #Mask containing the voxels in which analysis is performed.
outputdir #Directory to save all outputs (i.e., dMRI summary measures).
```

### 3. Run a GLM:
This steps runs a GLM on **each voxel** to compute the baseline measurements, the continuous change in each phenotype (w.r.t dMRI summary measure) and the noise covariance matrices. 

Here is an illustration of what's happening. We're computing the change of SM w.r.t continuous variables. 

![GLM](GLM.png)

```
bash bmrc_slurm_glm.sh
```
**Inputs in bash script:**
```
data #dMRI summary measure of different UKBB subjects.
mask #Mask containing the voxels in which analysis is performed.
designmat #Design matrix containing the phenotypes (seen in the image above).
outputdir #Directory to save all outputs.
```


### 4a. Inference (splitting jobs):
Inference is also performed on each voxel, across subjects. Since there are a lot of subjects, we split this into multiple jobs and run them in parallel.

Since this is a python file, this will need to be run in **win000** (for interactive running of scripts). See below for more details

```
python bmrc_splitting_inference.py
```

**Inputs in bash script:**
```
model #Trained change model used for inference.
glmdir #Directory containing the GLM outputs.
designmat #Design matrix containing the phenotypes (seen in the image above).
output #Directory to save all outputs.
full_mask #Mask containing the voxels to perform inference.
```

### 4b. Inference (collecting jobs):
The inference jobs are then collected.

Since this is also python file, this will also need to be run in **win000**.

```
python bmrc_collecting_inference.py
```
**Inputs in bash script: (same as bmrc_splitting_inference.py)**

## Important input files and descriptions:

As of now, here are some files that I used to generate my current results. Please feel free to ask if you have any questions on what's what.

```
#summary measures
data="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/dMRI/data.nii.gz" 
bvecs="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/dMRI/bvecs" 
bvals="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/dMRI/bvals" 
warp="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/TBSS/FA/dti_FA_to_MNI_warp.nii.gz" 
mask="/well/miller/users/afe677/UKBB/phenotype_analysis/wm_mask_for_MNI_eroded_v2.nii.gz" 

#GLM
dataset="large_scale"
summarydir="/well/miller/users/afe677/UKBB/phenotype_analysis/subset/summary_measures" #use the same one
mask="/well/miller/users/afe677/UKBB/phenotype_analysis/wm_mask_for_MNI_eroded_v2.nii.gz"

#inference
glmdir = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/GLM"
full_mask = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/GLM/valid_mask.nii.gz" 
```

### Design matrix 

designmat="/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/combined_design_matrix.npz"

```
/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/combined_design_matrix.npz
```
or 

```
/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/inference_order_phenotypes.npz
```
For the latter (i.e. inference_order_phenotypes.npz), it's the same design matrix, except the rows are ordered different.
Here, the phenotypes with the highest coefficient of determination (r^2), when correlated with dMRI data, are ordered first.

### Change model

```
/well/miller/users/afe677/UKBB/phenotype_analysis/change_models/invivo/normalisation_by_baseline/2D_2nd/SM_ball_sphere/watson_stick_zeppelin_sphere_ball_30000_samples_1.7_diff"
```
For more information on what biophysical model was used, and how it was trained, refer to training_SM_sphere_ball_invivo_2D.py. 

## Some tips on how to run things on BMRC: 

Here are some very basic instructions on to used

1. Once you're in WIN computing cluster, log into BMRC (change your_username)
```
ssh your_username@cluster1.bmrc.ox.ac.uk
```
2. SSH into into win000 for interactive jobs
```
ssh win000
```
3. Activate BENCH environment
```
module load Python/3.9.5-GCCcore-10.3.0 #load python module
module load FSL/6.0.5.1-foss-2021a #load fsl module
source example_path #path where the environment
cd example_directory #directory where continuous BENCH is
```

## Phenotypes and FUNPACK use

For a brief overview of which phenotypes were used, refer to the spreadsheet (UKBB_phenotypes.xlsx).

The phenotypes were processed used [FUNPACK](https://git.fmrib.ox.ac.uk/fsl/funpack), with some dedicated configurations.

**TODO** (FUNPACK configurations, and where they are in BMRC)

## How can I display them? 

**TODO** (jupyter notebook)
