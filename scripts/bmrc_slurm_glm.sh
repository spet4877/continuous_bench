#!/bin/bash

# This script is used to run a general linear model (GLM) analysis on the dMRI summary measures and phenotypes.

# Remove and create log_glm directory if present
#if [ -d "/well/miller/users/afe677/python/bench/scripts/log_glm" ]; then
#    rm -rf /well/miller/users/afe677/python/bench/scripts/log_glm
#fi
#mkdir /well/miller/users/afe677/python/bench/scripts/log_glm

#SBATCH --job-name=glm
#SBATCH --partition=short
#SBATCH --cpus-per-task=10
#SBATCH --output=/well/miller/users/afe677/python/bench/scripts/log_glm/%j.out
#SBATCH --error=/well/miller/users/afe677/python/bench/scripts/log_glm/%j.err


# Print some useful data about the job to help with debugging
echo "------------------------------------------------"
echo "SLURM Job ID: ${SLURM_JOB_ID}"
echo "Run on host: "`hostname`
echo "Operating system: "`uname -s`
echo "Username: "`whoami`
echo "Started at: "`date`
echo "------------------------------------------------"

# Inputs for the script
dataset="large_scale"
summarydir="/well/miller/users/afe677/UKBB/phenotype_analysis/subset/summary_measures" #use the same one
mask="/well/miller/users/afe677/UKBB/phenotype_analysis/wm_mask_for_MNI_eroded_v2.nii.gz"
designmat="/well/miller/users/afe677/UKBB/phenotype_analysis/$dataset/combined_design_matrix.npz"
outputdir="/well/miller/users/afe677/UKBB/phenotype_analysis/$dataset/GLM"

# Execution of script
bench continuous-glm --summarydir $summarydir --mask $mask --designmat $designmat --output $outputdir



# End of job script