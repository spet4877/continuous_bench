#!/usr/bin/env python

"""
This script takes the acquisition (bvalues, bvectors) files and output file path, and saves the change model, where appropriate

:param bval_path: path of the .txt bval files
:param bvec_path: path of the .txt bvec files
:param output_dir: directory to save the change models
:param num_samples: number of samples to train model with

:return change models for the model


"""
import os
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import argparse
from scipy import stats
import numpy as np
from bench import summary_measures
from bench import change_model
from bench import diffusion_models as dm


def main():
    p = argparse.ArgumentParser(description="Training standard model + ball + sphere for UKBB")

    """directories"""

    p.add_argument('-ad', '--acq_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='npz file with acquisition params')
    p.add_argument('-num', '--num_samples',
                   required=True, nargs='+', type=int,
                   help='Number of samples to train our model on')
    p.add_argument('-dc', '--diff_coeff',
                   required=True, nargs='+', type=float,
                   help='diffusion coefficient to train change model on')
    p.add_argument('-od', '--output_dir',
                   required=True, nargs='+', type=str, metavar='<str>',
                   help='directory to save trained model to')

    args = p.parse_args()
    args.acq_dir = ' '.join(args.acq_dir)
    args.output_dir = ' '.join(args.output_dir)

    output_dir = args.output_dir
    acq_npz = args.acq_dir

    num_samples = np.array(args.num_samples)[0]
    dif_coeff = np.array(args.diff_coeff)[0]

    ### === Acquisition parameters ===

    bvals = np.load(acq_npz)["a"]
    bvecs = np.load(acq_npz)["b"]
    pulse_duration = np.load(acq_npz)["c"]
    diffusion_time = np.load(acq_npz)["d"]
    gradient_strength = np.load(acq_npz)["e"]

    ### === Priors used ===
    priors = {('s_iso', 's_in', 's_ex', 's_sphere'): sample_signal, #2nd prior
                    'odi': stats.beta(a=2, b=6),
                    'd_iso': stats.truncnorm(loc=3, scale=.1, a=-3 / .1, b=np.Inf),
                    'd_a_in': stats.truncnorm(loc=dif_coeff, scale=.3, a=-dif_coeff / 0.3, b=np.Inf),
                    'd_a_ex': stats.truncnorm(loc=dif_coeff, scale=.3, a=-dif_coeff / 0.3, b=np.Inf),
                    'd_r_ex': stats.truncnorm(loc=dif_coeff / 2, scale=.3, a=-dif_coeff / 0.6, b=np.Inf),
                    'd_sphere': stats.truncnorm(loc=dif_coeff, scale=.3, a=-dif_coeff / 0.3, b=np.Inf),
                    'rad_sphere': stats.truncnorm(loc=5, scale=1, a=-5 / 0.3, b=np.Inf),
                    }


    ## Set up incomplete model

    diff_model = watson_noddi_with_sphere

    forward_model, sm_names = summary_decorator(model=diff_model,
                                                bval=bvals,
                                                bvec=bvecs,
                                                pulse_duration=pulse_duration,
                                                diffusion_time=diffusion_time,
                                                G=gradient_strength,
                                                summary_type='sh')

    tr = change_model.Trainer(forward_model, priors, summary_names=sm_names,
                              change_vecs=[{'s_iso': 1},
                                           {'s_in': 1},
                                           {'s_ex': 1},
                                           {'s_sphere': 1},
                                           {'odi': 1},
                                           {'d_iso': 1},
                                           {'d_a_in': 1},
                                           {'d_a_ex': 1},
                                           {'d_r_ex': 1},
                                           {'d_sphere': 1},
                                           {'rad_sphere': 1}])

    ch_mdl = tr.train_ml(n_samples=num_samples, verbose=True, parallel=False)

    ch_mdl.save(file_name="watson_stick_zeppelin_sphere_ball_{}_samples_{}_diff".format(num_samples,
                                                                                       dif_coeff), path=output_dir)


def watson_noddi_with_sphere(bval,
                             bvec,
                             pulse_duration,
                             diffusion_time,
                             G,
                             s_iso,
                             s_in,
                             s_ex,
                             s_sphere,
                             odi,
                             d_iso,
                             d_a_in,
                             d_a_ex,
                             d_r_ex,
                             d_sphere,
                             rad_sphere,
                             theta=0.,
                             phi=0.,
                             s0=1.):
    """
    Simulates diffusion signal with Watson dispressed NODDI model

    :param bval: b-values
    :param bvec: (,3) gradient directions(x, y, z)
    :param s_iso: signal fraction of isotropic diffusion
    :param s_in: signal fraction of intra-axonal diffusion
    :param s_ex: signal fraction of extra-axonal water
    :param d_iso: isotropic diffusion coefficient
    :param d_a_in: axial diffusion coefficient
    :param d_a_ex: axial diffusion coefficient for extra-axonal compartment
    :param tortuosity: ratio of radial to axial diffusivity
    :param odi: dispersion parameter of watson distribution
    :param theta: orientation of stick from z axis
    :param phi: orientation of stick from x axis
    :param s0: attenuation for b=0
    :return: (M,) diffusion signal
    """

    a_iso = dm.ball(bval=bval, bvec=bvec, d_iso=d_iso, s0=s_iso)

    a_int = dm.bingham_zeppelin(bval=bval, bvec=bvec, d_a=d_a_in, d_r=0,
                                odi=odi, odi2=odi,
                                psi=0, theta=theta, phi=phi, s0=s_in)

    a_ext = dm.bingham_zeppelin(bval=bval, bvec=bvec, d_a=d_a_ex,
                                d_r=d_r_ex,
                                odi=odi, odi2=odi,
                                psi=0, theta=theta, phi=phi, s0=s_ex)

    a_sphere = sphere(pulse_duration=pulse_duration,
                      diffusion_time=diffusion_time,
                      G=G,
                      radius=rad_sphere,
                      diffusivity=d_sphere,
                      S0=s_sphere)

    return (a_iso + a_int + a_ext + a_sphere) * s0


def sample_signal(n_samples):
    """
    samples signal fraction parameters for 3 compartment models.
    :param n_samples: number of required samples
    :return: samples (n_samples, 3)
    """
    tissue_type = np.random.choice(3, n_samples, p=[0.0, .2, .8])
    # 0: Pure CSF, 1: CSF partial volume , 2: brain tissue
    s_iso = stats.uniform(loc=0, scale=.9).rvs(n_samples)
    s_in = stats.truncnorm(loc=.45, scale=.2, a=-.4 / .2, b=1 / 0.2).rvs(n_samples)
    s_ex = stats.truncnorm(loc=.45, scale=.2, a=-.4 / .2, b=1 / 0.2).rvs(n_samples)
    s_sphere = stats.uniform(loc=0, scale=0.10).rvs(n_samples)

    # CSF:
    s_iso[tissue_type == 0] = 1 - 1e-4

    # inside brain (no CSF partial volume)
    s_iso[tissue_type == 2] = 1e-4

    norm = (1 - s_iso - s_sphere) / (s_in + s_ex)

    return s_iso, s_in * norm, s_ex * norm, s_sphere


def compute_GPDsum(am_r, pulse_duration, diffusion_time, diffusivity, radius):
    # print(diffusivity.shape)
    # print(am_r.shape)

    dam = diffusivity * am_r * am_r

    e11 = -dam * pulse_duration

    e2 = -dam * diffusion_time

    dif = diffusion_time - pulse_duration

    e3 = -dam * dif

    plus = diffusion_time + pulse_duration

    e4 = -dam * plus
    nom = 2 * dam * pulse_duration - 2 + (2 * np.exp(e11)) + (2 * np.exp(e2)) - np.exp(e3) - np.exp(e4)

    denom = dam ** 2 * am_r ** 2 * (radius ** 2 * am_r ** 2 - 2)

    return np.sum(nom / denom, 1)


# Constants:
gamma = 2.6752218744 * 1e8
# gamma = 42.577478518*1e6     # [sec]^-1 * [T]^-1
gamma_ms = gamma * 1e-3  # [ms]^-1 *[T]^-1

# From Camino source
#  60 first roots from the equation (am*x)j3/2'(am*x)- 1/2 J3/2(am*x)=0
am = np.array([2.08157597781810, 5.94036999057271, 9.20584014293667,
               12.4044450219020, 15.5792364103872, 18.7426455847748,
               21.8996964794928, 25.0528252809930, 28.2033610039524,
               31.3520917265645, 34.4995149213670, 37.6459603230864,
               40.7916552312719, 43.9367614714198, 47.0813974121542,
               50.2256516491831, 53.3695918204908, 56.5132704621986,
               59.6567290035279, 62.8000005565198, 65.9431119046553,
               69.0860849466452, 72.2289377620154, 75.3716854092873,
               78.5143405319308, 81.6569138240367, 84.7994143922025,
               87.9418500396598, 91.0842274914688, 94.2265525745684,
               97.3688303629010, 100.511065295271, 103.653261271734,
               106.795421732944, 109.937549725876, 113.079647958579,
               116.221718846033, 116.221718846033, 119.363764548757,
               122.505787005472, 125.647787960854, 128.789768989223,
               131.931731514843, 135.073676829384, 138.215606107009,
               141.357520417437, 144.499420737305, 147.641307960079,
               150.783182904724, 153.925046323312, 157.066898907715,
               166.492397790874, 169.634212946261, 172.776020008465,
               175.917819411203, 179.059611557741, 182.201396823524,
               185.343175558534, 188.484948089409, 191.626714721361])


def sphere(pulse_duration, diffusion_time, G, radius, diffusivity, S0):
    """
    Predicts signal of water restricted in  a sphere.
    Args:
        pulse_duration: applied gradient time (in ms)
        diffusion_time: diffusion time (in ms)
        G: gradient strength (in ?)
        radius: radius of sphere (in um)
        diffusivity: diffusion coefficient inside the sphere
        S0: base signal at b=0
    """
    ## === refactoring === ##

    pulse_duration = np.repeat(pulse_duration[np.newaxis, :], am.shape[0], axis=0)
    pulse_duration = np.repeat(pulse_duration[np.newaxis, :], diffusivity.shape[0], axis=0)

    diffusion_time = np.repeat(diffusion_time[np.newaxis, :], am.shape[0], axis=0)
    diffusion_time = np.repeat(diffusion_time[np.newaxis, :], diffusivity.shape[0], axis=0)

    diffusivity = np.repeat(diffusivity[:, np.newaxis], am.shape[0], axis=1)
    diffusivity = np.repeat(diffusivity[:, :, np.newaxis], diffusion_time.shape[-1], axis=2)

    radius = np.repeat(radius[:, np.newaxis], am.shape[0], axis=1)
    radius = np.repeat(radius[:, :, np.newaxis], diffusion_time.shape[-1], axis=2)

    S0 = np.repeat(S0[:, np.newaxis], diffusion_time.shape[-1], axis=1)

    ## === compute sum === ##

    G_T_per_micron = G * 1e-3 * 1e-6  # [T] * [um]^-1
    am_r = am[:, np.newaxis] / radius

    GPDsum = compute_GPDsum(am_r, pulse_duration, diffusion_time, diffusivity, radius)

    log_att = -2. * gamma_ms ** 2 * G_T_per_micron ** 2 * GPDsum

    return S0 * np.exp(log_att)


## === Modifield summary decorator to include pulse duration and different acquisition parameters === ##

def summary_decorator(model,
                      bval,
                      bvec,
                      pulse_duration,
                      diffusion_time,
                      G,
                      summary_type='sh', shm_degree=2):
    """
    Decorates a diffusion model to add noise and directly calculate a summary measurement. The return function can
    be used like sm = f(micro params, noise_std= default to zero).

    :param bvec: array of b-values
    :param bval: array of b-vectors
    :param model: a diffusion model (function)
    :param summary_type: either 'sh' (spherical harmonics), 'dt' (diffusion tensor model), 'sig' (raw signal)
    :param shm_degree: degree for spherical harmonics
    :return:
        function f that maps microstructural parameters to summary measurements, name of the summary measures
    """
    if summary_type == 'sh':
        def func(noise_std=0.0, **params):
            sig = model(bval, bvec, pulse_duration, diffusion_time, G, **params)
            noise = np.random.randn(*sig.shape) * noise_std
            sm = summary_measures.fit_shm(sig + noise, bval, bvec, shm_degree=shm_degree)
            return sm

        names = summary_measures.summary_names(bval, summarytype='sh', shm_degree=shm_degree)

    elif summary_type == 'sig':
        def func(noise_std=0.0, **params):
            sig = model(bval, bvec, pulse_duration, diffusion_time, G, **params)
            noise = np.random.randn(*sig.shape) * noise_std
            return sig + noise

        names = [f'{b:1.3f}-{g[0]:1.3f},{g[1]:1.3f},{g[2]:1.3f}' for b, g in zip(bval, bvec)]
    else:
        raise ValueError(f'Summary measurement type {summary_type} is not defined. Must be shm, dtm or sig.')
    func.__name__ = model.__name__
    return func, names


if __name__ == '__main__':
    main()
