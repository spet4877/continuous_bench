#!/bin/bash

##========================##

#directories
basename="change_models/invivo/normalisation_by_baseline/2D_2nd"
rootpath="/vols/Data/km/dkor/UKBB"
acqnpz=$rootpath/UKBB_acquisition_parameters.npz
logdir=/vols/Data/km/dkor/logfiles/$basename
outputdir=$rootpath/$basename/

##========================##
# book-keeping
mkdir -p $outputdir
r=1
# remove logfiles if present
if [ -d $logdir ]; then
    if [ $r -eq 1 ]; then
      rm -r $logdir
    else
      echo "Folder with log and text files already exists"
      echo "See $logdir or select remove with -r"
      exit 1
    fi
fi
mkdir -p $logdir

##========================##

#cluster parameters
chosenq=bigmem
r=1

##========================##
# Train

diff_coeff=1.7


##========================##
# Train

echo ""
echo "=== Training SM + sphere + ball ==="
echo ""

outputdir=$rootpath/$basename/SM_ball/
mkdir -p $outputdir

echo ""


num_samples=10000

echo "Samples: $num_samples"
echo "Diffusion coefficient: $diff_coeff"

echo python training_SM_sphere_ball_invivo_2D.py -ad $acqnpz -num $num_samples -dc $diff_coeff -od $outputdir >>$logdir/train_ukbb_models.txt;

echo ""


num_samples=30000

echo "Samples: $num_samples"
echo "Diffusion coefficient: $diff_coeff"

echo python training_SM_sphere_ball_invivo_2D.py -ad $acqnpz -num $num_samples -dc $diff_coeff -od $outputdir >>$logdir/train_ukbb_models.txt;

echo ""


fsl_sub -q $chosenq.q -l $logdir -t $logdir/train_ukbb_models.txt