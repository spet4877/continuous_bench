#!/usr/bin/env python3

"""
This script initiates parallel processing of large-scale inference, where we link change in phenotype to a change in dMRI data.
It is the first part of a two-part pipeline and is responsible for distributing workload across multiple SLURM jobs.

Workflow:
1. Configure and prepare paths for models, GLM directories, output locations, and phenotype data files.
2. Calculate the number of voxels to be processed and segment them into smaller batches.
3. For each batch, create a binary mask and submit a job to SLURM for parallel processing using the script bmrc_slurm_inference.sh

Usage:
- Run in rescomp to submit to SLURM

"""

# Import required packages
import os  # For interacting with the operating system
import subprocess
import numpy as np  # For numerical operations
from fsl.data.image import Image  # For working with image files
from tqdm import tqdm
from bench import change_model
from bench import image_io  # A function to read and write image files


# Function required to create directories, if necessary
def create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

    return folder


## 1. Configure and prepare paths for models, GLM directories, output locations, and phenotype data files.
# large scale (86 phenotypes, designated phenotypes with high R^2, confounds=770)
model = "/well/miller/users/afe677/UKBB/phenotype_analysis/change_models/invivo/normalisation_by_baseline/2D_2nd/SM_ball_sphere/watson_stick_zeppelin_sphere_ball_30000_samples_1.7_diff"
glmdir = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/GLM"
output = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/predictions"
designmat = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/inference_order_phenotypes.npz"  # sorted based on R^2
full_mask = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/GLM/valid_mask.nii.gz"

## 2. Calculate the number of voxels to be processed and segment them into smaller batches.
# Define the number of jobs
n_jobs = 200

# Output to check
print(f" === Inferring for {output}, with phenotype order taken from {designmat} === ")

# Define the phenotypes
phenotypes = np.load(designmat)["c"]

# Load change model
ch_mdl = change_model.ChangeModel.load(model)
model_names = ch_mdl.model_names

# Create a directory to store the binary masks for each sub-job
# os.makedirs(f'{output}/masks', exist_ok=True)
rootpath = os.path.dirname(output)
create_folder(f'{output}')  # create folder if needed
create_folder(f'{rootpath}/voxelwise_masks')
create_folder(f'{rootpath}/parallel_predictions')

# Determine the number of voxels in the full mask image
n_vox = (Image(full_mask).data > 0).sum()

## 3. For each batch, create a binary mask and submit a job to SLURM for parallel processing using the script
# bmrc_slurm_inference.sh
# Divide the voxels into n_jobs sub-jobs, and create a binary mask for each sub-job
per_job = n_vox // n_jobs + 1
jobs = []
for j in tqdm(range(n_jobs), desc='Building jobs'):

    # Create a binary mask for this sub-job
    idx = np.zeros((n_vox, 1))
    idx[(per_job * j):np.minimum(per_job * (j + 1), n_vox)] = 1

    image_io.write_nifti(idx, full_mask, f'{rootpath}/voxelwise_masks/{j}.nii.gz')

    # Command line interface here (i.e. this is where the inference for each batch of voxels are run):
    script_path = "/well/miller/users/afe677/python/bench/scripts/bmrc_slurm_inference.sh"
    command = f"sbatch {script_path} --m {model} --g {glmdir} --o {rootpath}/parallel_predictions/{j} --d {designmat} --a {rootpath}/voxelwise_masks/{j}.nii.gz"
    subprocess.run(command, shell=True)
