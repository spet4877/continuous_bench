#!/bin/bash

# This script is designed to be used on BMRC using SLURM.
# It processes diffusion MRI data to compute summary measures for multiple subjects in parallel.
# The script utilises the SLURM array jobs feature to handle a large number of subjects efficiently by
# specifying tasks with an array index corresponding to subject IDs.

# SLURM directives: Name of the job, partition on which to run the job, number of CPU cores per task, memory required per node, standard output path, standard error path, array range.

#SBATCH --job-name=sm
#SBATCH --partition=short
#SBATCH --cpus-per-task=3
#SBATCH --mem=8G
#SBATCH --output=/well/miller/users/afe677/python/bench/scripts/log/%j.out
#SBATCH --error=/well/miller/users/afe677/python/bench/scripts/log/%j.err
#SBATCH --array=1-34015

# Variables for subject list paths. Adjust the subject list as required for different analysis phases.
subjtextfile="/well/miller/users/afe677/UKBB/phenotype_analysis/analysis_v2/analysis_subject_list.txt"

# Load the list of subjects to be processed.
subjarr=();
while IFS= read -r line || [[ "$line" ]];
 do   subjarr+=("$line");
done < $subjtextfile

# Print job and system details for logging and debugging purposes.
echo "------------------------------------------------"
echo "SLURM Job ID: $SLURM_JOB_ID"
echo "Current task ID: $SLURM_ARRAY_TASK_ID"
echo "Run on host: $(hostname)"
echo "Operating system: $(uname -s)"
echo "Username: $(whoami)"
echo "Started at: $(date)"
echo "------------------------------------------------"

# Determine the subject based on the task array ID.
subj=${subjarr[${SLURM_ARRAY_TASK_ID}-1]}
echo "Processing Subject: $subj"
echo "Task Index: $SLURM_ARRAY_TASK_ID"
echo " "

# Set paths to the subject's imaging data.
data="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/dMRI/data.nii.gz" #The raw dMRI data
bvecs="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/dMRI/bvecs" #b-vectors (dMRI parameter)
bvals="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/dMRI/bvals" #b-values (dMRI parameter)
warp="/well/win-biobank/projects/imaging/data/data3/subjectsAll/$subj/dMRI/TBSS/FA/dti_FA_to_MNI_warp.nii.gz" #warp field
mask="/well/miller/users/afe677/UKBB/phenotype_analysis/wm_mask_for_MNI_eroded_v2.nii.gz" #WM mask to exclude non-WM voxels

# Set the output directory for the summary measures.
outputdir="/well/miller/users/afe677/UKBB/phenotype_analysis/analysis_v2/summary_measures/$subj"

# Run the diffusion summary measure computation using BENCH.
bench diff-summary --data $data --bvecs $bvecs --bvals $bvals --mask $mask --xfm $warp --output $outputdir --shm-degree 2 --summarytype sh --b0-thresh 0.1 --normalise
