#!/bin/bash


while getopts "m:g:o:d:a:" opt
do
   case "$opt" in
      m ) model="$OPTARG" ;;
      g ) glmdir="$OPTARG" ;;
      o ) output="$OPTARG" ;;
      d ) designmat="$OPTARG" ;;
      a ) mask="$OPTARG" ;;
      ? ) echo "parameters invalid" ;; # Print instructions in case parameter is non-existent
   esac
done

# Specify a job name
#SBATCH --job-name="inference"

# --- Parameters for the Queue Master ---

# Project name and target queue
#SBATCH --partition=long

# Run the job in the current working directory
#SBATCH --chdir=./

# Log locations which are relative to the current
# working directory of the submission
#SBATCH --output=/well/miller/users/afe677/python/bench/scripts/log_inference_parallel/%j.out
#SBATCH --error=/well/miller/users/afe677/python/bench/scripts/log_inference_parallel/%j.err

# Request N slots for compute
#SBATCH --cpus-per-task=3

# Request scheduler to make a copy of the user's shell environment at the time of submission
#SBATCH --export=ALL

# Print some useful data about the job to help with debugging
echo "------------------------------------------------"
echo "SLURM Job ID: $SLURM_JOB_ID"
echo "Run on host: "`hostname`
echo "Operating system: "`uname -s`
echo "Username: "`whoami`
echo "Started at: "`date`
echo "------------------------------------------------"

bench continuous-inference --model $model --glmdir $glmdir --output $output --designmat $designmat --mask $mask

