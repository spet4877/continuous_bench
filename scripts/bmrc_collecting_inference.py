#!/usr/bin/env python3

"""
This script is the second part of a two-part pipeline for large-scale phenotype analysis.
It aggregates and validates the results from multiple SLURM jobs processed by the first script.
The main purpose is to ensure data integrity, completeness, and prepare for final analysis or reporting.

Workflow:
1. Check directories for valid output data, ensuring that each contains expected results files.
2. Aggregate, check and write results from all distributed jobs
"""

## Run in win000

import os  # For interacting with the operating system

# Import required packages
import numpy as np  # For numerical operations
from fsl.data.image import Image  # For working with image files
from tqdm import tqdm

from bench import change_model
from bench import image_io  # A function to read and write image files


def create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)

    return folder

def phenotype_dir_checker(root_dir):
    """
    Scans a directory for numbered subdirectories (1-999), identifies phenotype subdirectories within them,
    and checks for the presence of .nii.gz files. Returns a list of phenotype directories that consistently
    contain these files across all numbered directories.

    Args:
        root_dir (str): The path to the root directory containing the numbered directories.

    Returns:
        list: A list of phenotype directory names present in all valid numbered directories and contain .nii.gz files.

    """

    numbered_dirs = []
    for dirpath, dirnames, filenames in os.walk(root_dir):
        # Check if the directory name is a number between 1 and 999
        if os.path.basename(dirpath).isdigit() and 1 <= int(os.path.basename(dirpath)) <= 999:
            numbered_dirs.append(dirpath)

    for idx, numbered_dir in enumerate(numbered_dirs):

        # find all phenotype directories in a each numbered directory
        phenotype_dirs = [dir_name for dir_name in os.listdir(numbered_dir) if
                          os.path.isdir(os.path.join(numbered_dir, dir_name))]

        phenotypes_with_nii_files = []

        # find if each phenotype directory has a nii.gz file
        for phenotype_dir in phenotype_dirs:

            nii_files = [file_name for file_name in os.listdir(os.path.join(numbered_dir, phenotype_dir)) if
                         file_name.endswith('.nii.gz')]

            # keep the phenotype directory if there is a nii.gz file
            if len(nii_files) > 0:
                phenotypes_with_nii_files.append(phenotype_dir)

        # keep these phenotypes that is found in ALL numbered directory
        if idx == 0:
            reference_phenotype_dir = phenotypes_with_nii_files
        else:
            reference_phenotype_dir = list(set(reference_phenotype_dir).intersection(set(phenotypes_with_nii_files)))

    return reference_phenotype_dir


# Define input and output paths and other parameters model =
## large scale (86 phenotypes, designated phenotypes with high R^2, confounds=770)
model = "/well/miller/users/afe677/UKBB/phenotype_analysis/change_models/invivo/normalisation_by_baseline/2D_2nd/SM_ball_sphere/watson_stick_zeppelin_sphere_ball_30000_samples_1.7_diff"
glmdir = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/GLM"
output = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/predictions"
designmat="/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/inference_order_phenotypes.npz" #sorted based on R^2
full_mask = "/well/miller/users/afe677/UKBB/phenotype_analysis/large_scale/GLM/valid_mask.nii.gz"

# Define the number of jobs

n_jobs = 200

# Load change model
ch_mdl = change_model.ChangeModel.load(model)
model_names = ch_mdl.model_names
model_names = ["nochange" if x == "[]" else x for x in model_names]

# Create a directory to store the binary masks for each sub-job
rootpath = os.path.dirname(output)

create_folder(f'{output}')  # create folder if needed
create_folder(f'{rootpath}/voxelwise_masks')
create_folder(f'{rootpath}/parallel_predictions')

# Define the phenotypes
#phenotypes = np.load(designmat)["c"]

phenotypes = phenotype_dir_checker(f'{rootpath}/parallel_predictions')

# Determine the number of voxels in the full mask image
n_vox = (Image(full_mask).data > 0).sum()

# Divide the voxels into n_jobs sub-jobs, and create a binary mask for each sub-job
per_job = n_vox // n_jobs + 1

# Combine the posterior probabilities and amounts from all sub-jobs

for i, phenotype in enumerate(phenotypes):

    print(f"== Phenotype {phenotype} == ")

    if os.path.exists(f'{output}/{phenotype}'):
        print(f"{phenotype} exists")
        continue

    # Initialize numpy arrays (for each phenotype) to store posteriors and amounts
    posteriors = np.zeros((n_vox, len(model_names)))
    amounts = np.zeros((n_vox, len(model_names)))
    predictions = np.zeros((n_vox))

    for j in tqdm(range(n_jobs), desc='Collecting results'):

        # Read in the posterior probabilities and amounts for this sub-job
        p, a, prediction = image_io.read_continuous_inference_results(
            f'{rootpath}/parallel_predictions/{j}/{phenotype}',
            phenotype,
            mask_add=f'{rootpath}/voxelwise_masks/{j}.nii.gz')  # cluster of voxels from that job

        # Append the results to the existing arrays
        predictions[(per_job * j):np.minimum(per_job * (j + 1), n_vox)] = prediction

        for k, m in enumerate(model_names):
            posteriors[(per_job * j):np.minimum(per_job * (j + 1), n_vox), k] = p[m]
            amounts[(per_job * j):np.minimum(per_job * (j + 1), n_vox), k] = a[m]

    # save the results:
    image_io.write_continuous_inference_results(path=f'{output}/{phenotype}',
                                                model_names=model_names,
                                                predictions=predictions,
                                                posteriors=posteriors,
                                                peaks=amounts,
                                                mask=f'{full_mask}',
                                                variable=phenotype)


